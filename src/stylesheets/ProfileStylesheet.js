import { StyleSheet } from "react-native";

export default StyleSheet.create({
    FlatListItemSeperator: {
        height: 1,
        width: "100%",
        backgroundColor: "#607D8B",
    },
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    TextInputLayout: {
        minHeight: 45,
        color: '#323232',
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 2,
        borderWidth: 0.6,
        borderColor: '#dddddd',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    TextInputLayoutContent: {
        minHeight: 45,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 2,
        borderWidth: 0.6,
        borderColor: '#dddddd',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    buttonHover: {
        width: 150,
        height: 50,
        backgroundColor: "#3fabf3",
        borderBottomColor: "#3fabf3",
        marginLeft: 15,
        borderWidth: 0,
        marginTop: 5,
        shadowColor: "rgba(0, 0, 0, 0.1)",
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        marginBottom: 25,
        shadowOffset: { width: 1, height: 13 },
        fontSize: 13,
        borderRadius: 4,
        overflow: "hidden"
    },
    UpdateButtonText: {
        color: '#fff',
        justifyContent: 'center',
        fontSize: 16,
        top: 20
    },
    UpdateButtonTouchableOpacity: {
        backgroundColor: '#3d4461',
        height: 60,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        elevation: 3,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.2,
        shadowColor: "#000",
    },
    ProfileLoadingView: {
        justifyContent: "center",
        height: "100%"
    },
    ProfileLoadingActivityIndicator: {
        height: 30,
        width: 30,
        borderRadius: 60,
        alignContent: "center",
        alignSelf: "center",
        justifyContent: "center",
        backgroundColor: "#fff",
        elevation: 5
    },
    ProfileScrollView: {
        backgroundColor: '#fff',
        margin: 10,
        borderRadius: 4,
        borderRadius: 4,
        elevation: 3,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.2,
        shadowColor: "#000",
    },
    ProfileHeaderText: {
        color: '#3d4461',
        fontSize: 20,
        fontWeight: "900",
        marginBottom: 15,
        marginLeft: 10,
        marginTop: 10
    },
    PlaceholderTextColor: {
        color: "#7F7F7F"
    },
    DescriptionTextLayout: {
        paddingLeft: 10,
        height: 150,
        alignItems: 'flex-start',
        borderRadius: 2,
        borderWidth: 0.6,
        borderColor: '#dddddd',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    LocationView: {
        backgroundColor: '#fff',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        borderRadius: 4,
        elevation: 3,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.2,
        shadowColor: "#000", borderRadius: 4,
    },
    LocationText: {
        color: '#3d4461',
        fontSize: 20,
        fontWeight: "900",
        marginBottom: 15,
        marginLeft: 10,
        marginTop: 10
    },
    UploadParentView: {
        backgroundColor: '#fff',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        borderRadius: 4,
        elevation: 3,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.2,
        shadowColor: "#000", borderRadius: 4,
    },
    UploadChildView: {
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        overflow: 'hidden'
    },
    UploadText: {
        color: '#3d4461',
        fontSize: 20,
        fontWeight: "900",
        marginBottom: 15,
        marginLeft: 10,
        marginTop: 10
    },
    UploadTouchableOpacity: {
        flexDirection: 'row',
        justifyContent: 'center',
        borderRadius: 4,
        borderStyle: 'dashed',
        borderColor: '#dddddd',
        borderWidth: 0.6,
        height: 150,
        width: '60%',
        marginBottom: 10
    },
    AddFileView: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    AddFileText: {
        color: '#767676',
        fontSize: 17
    },
    File: {
        width: '40%',
        height: 150,
        borderRadius: 4
    },
    MembershipParentView: {
        backgroundColor: '#fff',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        borderRadius: 4,
        elevation: 3,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowColor: "#000", borderRadius: 4,
    },
    MembershipChildView: {
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    MembershipText: {
        color: '#3d4461',
        fontSize: 20,
        fontWeight: "900",
        marginBottom: 15,
        marginLeft: 10,
        marginTop: 10
    },
    MembershipTextInput: {
        paddingLeft: 10,
        borderRadius: 2,
        height: 50,
        color: '#323232',
        borderWidth: 0.6,
        borderColor: '#dddddd',
        marginBottom: 10,
        width: '80%'
    },
    MembershipIconView: {
        backgroundColor: '#3d4461',
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        height: 50,
        width: '20%',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    MembershipTop: {
        top: 15
    },
    MembershipDataView: {
        borderWidth: 0,
        margin: 2,
        flexDirection: "column",
        paddingTop: 12,
        paddingBottom: 12,
        backgroundColor: "#f7f7f7"
    },
    MembershipDataText: {
        color: '#484848',
        fontSize: 15,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 2,
        fontWeight: '400',
        marginBottom: 5
    },
    TextInput: {
        height: 45,
        paddingLeft: 10,
        borderRadius: 2,
        borderWidth: 0.6,
        borderColor: "#dddddd",
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10
    },
    TextInputParentView: {
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5
    },
    TextInputChildView: {
        marginTop: 10
    },
    TouchableOpacity: {
        width: '30%',
        marginBottom: 15,
        marginLeft: 10,
        marginTop: 15
    },
    Text: {
        color: '#3d4461',
        width: '70%',
        fontSize: 20,
        fontWeight: "900",
        marginBottom: 15,
        marginLeft: 10,
        marginTop: 10
    },
    ParentView: {
        backgroundColor: '#fff',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        borderRadius: 4,
        elevation: 3,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowColor: "#000", borderRadius: 4,
    },
    ChildView: {
        flexDirection: 'row'
    },
    AddNowButton: {
        color: "#fff",
        fontSize: 14,
        fontWeight: "500",
        textAlign: "center",
        top: 18
    },
    CollapseHeader: {
        flexDirection: 'row',
        marginTop: 3,
        height: 50,
        borderWidth: 0,
        margin: 0,
        flexDirection: "column",
        backgroundColor: "#f7f7f7"
    },
    CollapseHeaderView: {
        flexDirection: 'row',
        borderWidth: 0,
        backgroundColor: "#f7f7f7"
    },
    CollapseHeaderText: {
        paddingLeft: 10,
        borderRadius: 2,
        height: 50,
        borderWidth: 0.6,
        borderColor: '#dddddd',
        paddingTop: 15,
        width: '70%'
    },
    EditIconView: {
        backgroundColor: '#3d4461',
        height: 50,
        width: '15%',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    DeleteIconView: {
        backgroundColor: '#ff5851',
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        height: 50,
        width: '15%',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    AddDocument: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    DocumentView: {
        height: 50,
        width: '15%',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    AddDocumentView: { flexDirection: 'row', justifyContent: 'center', borderRadius: 4, marginLeft: 2, marginRight: 2, borderStyle: 'dashed', borderColor: '#dddddd', borderWidth: 1, height: 150, width: '100%', marginBottom: 10 },
    DocumentTextView: {
        flexDirection: 'column',
        width: '70%'
    },
    DocumentText: {
        paddingLeft: 10,
        height: 50,
        textAlignVertical: 'center'
    },
    DocumentIcon: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2
    },
    DocumentIconView: { height: 50, width: '15%', justifyContent: 'center', flexDirection: 'row' }
});