import { StyleSheet } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#ffffff',
		marginTop: 2,
		elevation: 3,
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.2,
		shadowColor: '#000',
		marginRight: 1,
		marginBottom: 5,
		borderRadius: 4,
	},
	iconView: {
		flexDirection: 'row',
		width: '40%',
		alignItems: 'center',
		marginRight: 8,
	},
	iconText: { marginLeft: 3, color: '#767676', fontSize: 12 },
	imageView: {
		backgroundColor: '#fff',
		borderRadius: 4,
		overflow: 'hidden',
	},
	parentView: { flexDirection: 'column', marginTop: 2 },
	childView: { flexDirection: 'row', width: '100%' },
	textView: { color: '#484848', fontSize: 15, fontWeight: '500' }
});