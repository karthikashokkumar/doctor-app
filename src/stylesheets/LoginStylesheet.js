import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	Container: {
		height: '80%',
		justifyContent: "center",
		alignItems: "center"
	},
	LoadMoreButton: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 20,
		paddingRight: 20,
		backgroundColor: "#3d4461",
		borderRadius: 4,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	LoginButtonText: {
		color: 'white',
		fontSize: 15,
		textAlign: 'center',

	},
	CustomProgressBarParentView: {
		flex: 1,
		backgroundColor: '#dcdcdc',
		alignItems: 'center',
		justifyContent: 'center',
		position: 'relative'
	},
	CustomProgressBarChildView: {
		borderRadius: 10,
		backgroundColor: 'white',
		padding: 25,
		position: 'absolute'
	},
	CustomProgressBarText: {
		fontSize: 20,
		fontWeight: '200'
	},
	SignupText: {
		textAlign: "center",
		color: "#3d4461",
		fontSize: 17,
		margin: 10,
	},
	ForgotPasswordText: {
		textAlign: "center",
		alignSelf: "center",
		color: "#616161",
		fontSize: 15,
		margin: 10
	},
	TextInputStyle: {
		fontSize: 15,
		padding: 5,
		height: 40,
		color: '#323232'
	},
	BottomView: {
		borderBottomColor: "#dddddd",
		borderBottomWidth: 0.6
	},
	LoginFormView: {
		width: "90%",
		borderWidth: 0.6,
		borderRadius: 4,
		margin: 10,
		borderColor: '#dddddd'
	},
	BannerLogo: {
		width: 150,
		height: 80,
		resizeMode: "center",
		alignSelf: "center"
	},
	LoginHeaderText: {
		fontSize: 18,
		fontWeight: "500",
		color: "#fff",
		height: 30,
		marginTop: 9
	},
	LoginHeaderParentView: {
		flexDirection: "column",
		width: "60%",
		display: "flex",
		alignContent: "center",
		alignSelf: "center",
		justifyContent: "center"
	},
	LoginHeaderChildView: {
		flexDirection: "row",
		display: "flex",
		alignSelf: "center"
	},
	TouchableOpacity: {
		flexDirection: "column",
		width: "20%",
		display: "flex",
		alignContent: "center",
		alignSelf: "center",
		justifyContent: "center"
	},
	LoginHeaderView: {
		height: 60,
		paddingLeft: 15,
		paddingRight: 15,
		width: "100%",
		backgroundColor: "#3d4461",
		flexDirection: "row",
		shadowOffset: { width: 0, height: 2 },
		shadowColor: "#000",
		shadowOpacity: 0.2,
		shadowRadius: 2,
		elevation: 10
	}
});