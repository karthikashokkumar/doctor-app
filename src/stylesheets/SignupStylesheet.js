import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    HeaderView: {
        height: 60,
        paddingLeft: 15,
        paddingRight: 15,
        width: "100%",
        backgroundColor: "#3d4461",
        flexDirection: "row",
        shadowOffset: { width: 0, height: 2 },
        shadowColor: "#000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 10
    },
    HeaderTouchableOpacity: {
        flexDirection: "column",
        width: "20%",
        display: "flex",
        alignContent: "center",
        alignSelf: "center",
        justifyContent: "center"
    },
    HeaderParentView: {
        flexDirection: "column",
        width: "60%",
        display: "flex",
        alignContent: "center",
        alignSelf: "center",
        justifyContent: "center"
    },
    HeaderChildView: {
        flexDirection: "row",
        display: "flex",
        alignSelf: "center"
    },
    HeaderText: {
        fontSize: 18,
        fontWeight: "500",
        color: "#fff",
        height: 30,
        marginTop: 9
    },
    BannerLogo: {
        width: 150,
        height: 80,
        resizeMode: "center",
        alignSelf: "center",
        marginTop: 30
    },
    RegisterAsView: {
        height: 65,
        flexDirection: "column",
        justifyContent: "center",
        margin: 15,
        backgroundColor: "#fcfcfc",
        borderLeftWidth: 5,
        borderLeftColor: "#3d4461"
    },
    RegisterAsText: {
        marginLeft: 10,
        fontSize: 20,
        fontWeight: "700",
        color: "#000000",
    },
    StartAsRadio: {
        paddingTop: 0,
        flexDirection: "row",
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 10,
        display: "flex",
        width: "100%",
        alignSelf: "center",
        alignContent: "center",
        textAlign: "center"
    },
    PersonalDetailsView: {
        height: 65,
        flexDirection: "column",
        justifyContent: "center",
        margin: 15,
        backgroundColor: "#fcfcfc",
        borderLeftWidth: 5,
        borderLeftColor: "#3d4461"
    },
    PersonalDetailsInputView: {
        borderWidth: 0.6,
        borderRadius: 4,
        margin: 10,
        borderColor: '#dddddd'
    },
    PersonalDetailsText: {
        marginLeft: 10,
        fontSize: 20,
        fontWeight: "700",
        color: "#000000",
    },
    ViewBottomStyle: {
        borderBottomColor: "#dddddd",
        borderBottomWidth: 0.6
    },
    FlexDirectionRow: {
        flexDirection: 'row'
    },
    TextInputStyle: {
        fontSize: 17,
        color: '#323232',
        height: 45,
        marginLeft: 5,
        width: '90%'
    },
    YourLocationView: {
        height: 65,
        flexDirection: "column",
        justifyContent: "center",
        margin: 15,
        backgroundColor: "#fcfcfc",
        borderLeftWidth: 5,
        borderLeftColor: "#3d4461"
    },
    YourLocationText: {
        marginLeft: 10,
        fontSize: 20,
        fontWeight: "700",
        color: "#000000",
    },
    MultiSelectMainWrapper: {
        backgroundColor: '#fff',
        borderRadius: 4,
        marginTop: 10
    },
    MultiSelectDropdownMenu: {
        backgroundColor: '#fff',
        paddingRight: -7,
        height: 55,
        paddingLeft: 10,
        borderWidth: 0.6,
        borderColor: '#fff',
        borderColor: '#dddddd',
        borderRadius: 4
    },
    ContinueTouchableOpacity: {
        alignItems: "center",
        height: 40,
        margin: 10,
        borderRadius: 4,
        width: "50%",
        alignSelf: "center",
        backgroundColor: "#3d4461"
    },
    SignupButton: {
        alignSelf: "center",
        alignItems: "center",
        textAlign: "center",
        color: "#fff",
        paddingTop: 10
    },
    SigninView: {
        backgroundColor: "#3d4461",
        height: 45,
        width: "100%",
        marginTop: 10
    },
    SigninText: {
        color: "#fff",
        alignSelf: 'center',
        fontSize: 17,
        top: 12
    }
});