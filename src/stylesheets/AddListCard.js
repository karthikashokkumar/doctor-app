import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
		flex: 1,
		backgroundColor: "#f7f7f7"
	},
	buttonHover: {
		width: 150,
		height: 50,
		backgroundColor: "#3fabf3",
		borderBottomColor: "#3fabf3",
		marginLeft: 15,
		borderWidth: 0,
		marginTop: 5,
		shadowColor: "rgba(0, 0, 0, 0.1)",
		shadowOpacity: 0.8,
		elevation: 6,
		shadowRadius: 15,
		marginBottom: 25,
		shadowOffset: { 
			width: 1, 
			height: 13 
		},
		fontSize: 13,
		borderRadius: 4,
		overflow: "hidden"
	},
	textInput: {
		height: 45,
		paddingLeft: 10,
		borderRadius: 2,
		borderWidth: 0.6,
		borderColor: "#dddddd",
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10
	},
	descriptionTextInput: {
		height: 45,
		paddingLeft: 10,
		height: 150,
		alignItems: "flex-start",
		borderRadius: 2,
		borderWidth: 0.6,
		borderColor: "#dddddd",
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10
	},
	touchableOpacity: {
		color: "#fff",
		fontSize: 14,
		fontWeight: "500",
		textAlign: "center",
		top: 18
	},
	collapseHeader: { 
		flexDirection: 'row', 
		borderWidth: 0, 
		margin: 5, 
		flexDirection: "column", 
		backgroundColor: "#f7f7f7" 
	},
	textView: { 
		flexDirection: 'row', 
		borderWidth: 0, 
		backgroundColor: "#f7f7f7" 
	},
	editIconView: { 
		backgroundColor: '#3d4461', 
		height: 50, 
		width: '15%', 
		justifyContent: 'center', 
		flexDirection: 'row' 
	},
	deleteIconView: { 
		backgroundColor: '#ff5851', 
		borderTopRightRadius: 2, 
		borderBottomRightRadius: 2, 
		height: 50, 
		width: '15%', 
		justifyContent: 'center', 
		flexDirection: 'row' 
	},
	textExperience:{ 
		paddingLeft: 10, 
		borderRadius: 2, 
		height: 50, 
		borderWidth: 0.6, 
		borderColor: '#dddddd', 
		paddingTop: 15, 
		width: '70%' 
	}
});