import axios from "axios";
import React, { Component } from "react";
import { RadioGroup } from "react-native-btr";
import * as Constants from '../config/Constants';
import MultiSelect from "react-native-multiple-select";
import { ScrollView } from "react-native-gesture-handler";
import AntIcon from "react-native-vector-icons/AntDesign";
import { View, TouchableOpacity, Text, TextInput, Alert, Image, StatusBar, BackHandler } from "react-native";
import SignupStyles from "../stylesheets/SignupStylesheet";

class SignupScreen extends Component {
	constructor() {
		super();
		this.showFilters = true;
		this.state = this.getInitialState();
		this.handleSystemBackButton = this.handleSystemBackButton.bind(this);
	}

	getInitialState = () => ({
			isLoading: true,
			projectLocationKnown: "",
			FirstName: "",
			LastName: "",
			Email: "",
			Password: "",
			RetypePassword: "",
			Phone: "",
			radioButtonsforStartAs: [
				{
					label: Constants.SignupHospital,
					value: "hospital",
					checked: true,
				},
				{
					label: Constants.SignupDoctor,
					value: "doctor",
					checked: false,
				},
				{
					label: Constants.SignupRegularUser,
					value: "regular",
					checked: false,
				}
			]
	});

	componentDidMount() {
		this.ProjectLocationSpinner();
		BackHandler.addEventListener('hardwareBackPress', this.handleSystemBackButton);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleSystemBackButton);
	}

	handleSystemBackButton () {
		this.props.navigation.goBack(null);
		return true;
	}

	ProjectLocationSpinner = async () => {
		let method = "GET";
		let headers = { Accept: "application/json", "Content-Type": "application/json" }
		return fetch(Constants.BaseUrl + "taxonomies/get_list?list=location", { method, headers })
			.then(response => response.json())
			.then(responseJson => { this.setState({ projectLocation: responseJson }); })
			.catch(error => { console.error(error) });
	};

	CreateAccount = () => {
		let selectedItemtype = this.state.radioButtonsforStartAs.find(e => e.checked == true);
		const { projectLocationKnown, FirstName, LastName, Email, Password, RetypePassword, Phone } = this.state;
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (FirstName == "" && LastName == "" && Email == "" && Phone == "" && Password == "" && RetypePassword == "") {
			alert("Please enter all details");
			this.setState({ email: "Please enter all details" });
		} else if (reg.test(Email) === false) {
			alert("Invalid Email Entered");
			this.setState({ email: "Invalid Email Entered" });
			return false;
		} else if (Password !== RetypePassword) {
			alert("Passwords does not match");
		} else {
			let body = {
				first_name: FirstName,
				last_name: LastName,
				email: Email,
				password: Password,
				verify_password: RetypePassword,
				mobile: Phone,
				role: selectedItemtype.value,
				locations: projectLocationKnown[0],
			};
			console.log('Sign up Body: ', body);
			axios.post(Constants.BaseUrl + "user/signup", body)
				.then(async response => {
					if (response.status === 200) {
						Alert.alert(`${selectedItemtype.label} Registered.`);
						this.props.navigation.navigate('LoginScreen', { user_id: response.data.user_id, email: Email, password: Password });
					} else if (response.status === 203) {
						Alert.alert("Error", JSON.stringify(response));
					}
				})
				.catch(error => {
					console.log(error);
				});
		}
	};

	render() {
		let selectedItemforStartAs = this.state.radioButtonsforStartAs.find(e => e.checked == true);
		selectedItemforStartAs = selectedItemforStartAs	? selectedItemforStartAs.value	: this.state.radioButtonsforStartAs[0].value;
		return (
			<View style={SignupStyles.Container}>
				<StatusBar backgroundColor="#3d4461" barStyle="default" />
				<View style={SignupStyles.HeaderView}>
					<TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={SignupStyles.HeaderTouchableOpacity}>
						<AntIcon name="back" size={25} color={"#fff"} />
					</TouchableOpacity>
					<View style={SignupStyles.HeaderParentView}>
						<View style={SignupStyles.HeaderChildView}>
							<Text style={SignupStyles.HeaderText}>Register Now</Text>
						</View>
					</View>
				</View>
				<ScrollView>
					<Image style={SignupStyles.BannerLogo} source={require("../../Assets/Images/SplashImage.png")} />
					<View style={SignupStyles.RegisterAsView}>
						<Text style={SignupStyles.RegisterAsText}>Register As</Text>
					</View>
					<View style={{ marginLeft: 10 }}>
						<RadioGroup
							color={Constants.primaryColor}
							labelStyle={{ fontSize: 14 }}
							radioButtons={this.state.radioButtonsforStartAs}
							onPress={radioButtons => this.setState({ radioButtons })}
							style={SignupStyles.StartAsRadio}
						/>
					</View>
					<View style={SignupStyles.PersonalDetailsView}>
						<Text style={SignupStyles.PersonalDetailsText}>Personal Details</Text>
					</View>
					<View style={SignupStyles.PersonalDetailsInputView}>
						<TextInput
							style={SignupStyles.TextInputStyle}
							underlineColorAndroid="transparent"
							editable={true}
							placeholderTextColor="#999999"
							onChangeText={FirstName => this.setState({ FirstName })}
							placeholder={Constants.SignupFname}>
						</TextInput>
						<View style={SignupStyles.ViewBottomStyle} />
						<TextInput
							style={SignupStyles.TextInputStyle}
							underlineColorAndroid="transparent"
							editable={true}
							placeholderTextColor="#999999"
							onChangeText={LastName => this.setState({ LastName })}
							placeholder={Constants.SignupLname}>
						</TextInput>
						<View style={SignupStyles.ViewBottomStyle} />
						<View style={SignupStyles.FlexDirectionRow}>
							<TextInput
								style={SignupStyles.TextInputStyle}
								underlineColorAndroid="transparent"
								editable={true}
								placeholderTextColor="#999999"
								autoCompleteType="email"
								onChangeText={Email => this.setState({ Email })}
								placeholder={Constants.SignupEmail}></TextInput>
							<AntIcon name="mail" size={15} color={"#999999"} style={{ top: 15 }} />
						</View>
						<View style={SignupStyles.ViewBottomStyle} />
						<View style={SignupStyles.FlexDirectionRow}>
							<TextInput
								style={SignupStyles.TextInputStyle}
								underlineColorAndroid="transparent"
								editable={true}
								placeholderTextColor="#999999"
								autoCompleteType="tel"
								onChangeText={Phone => this.setState({ Phone })}
								placeholder={"Mobile Number"}></TextInput>
							<AntIcon name="phone" size={15} color={"#999999"} style={{ top: 15 }} />
						</View>
						<View style={SignupStyles.ViewBottomStyle} />
						<View style={SignupStyles.FlexDirectionRow}>
							<TextInput
								style={SignupStyles.TextInputStyle}
								underlineColorAndroid="transparent"
								editable={true}
								placeholderTextColor="#999999"
								autoCompleteType="password"
								secureTextEntry={true}
								onChangeText={Password => this.setState({ Password })}
								placeholder={Constants.SignupPassword}></TextInput>
							<AntIcon name="lock" size={15} color={"#999999"} style={{ top: 15 }} />
						</View>
						<View style={SignupStyles.ViewBottomStyle} />
						<View style={SignupStyles.FlexDirectionRow}>
							<TextInput
								style={SignupStyles.TextInputStyle}
								underlineColorAndroid="transparent"
								editable={true}
								placeholderTextColor="#999999"
								autoCompleteType="password"
								secureTextEntry={true}
								onChangeText={RetypePassword => this.setState({ RetypePassword })}
								placeholder={Constants.SignupRetypePassword}></TextInput>
							<AntIcon name="lock" size={15} color={"#999999"} style={{ top: 15 }} />
						</View>
					</View>
					<View style={SignupStyles.YourLocationView}>
						<Text style={SignupStyles.YourLocationText}>Select Your Location</Text>
					</View>
					<View style={{ marginLeft: 15, marginRight: 15 }}>
						<MultiSelect
							ref={component => { this.multiSelect = component; }}
							onSelectedItemsChange={value => this.setState({ projectLocationKnown: value })}
							uniqueKey="id"
							items={this.state.projectLocation}
							selectedItems={this.state.projectLocationKnown}
							borderBottomWidth={0}
							single={true}
							searchInputPlaceholderText="Search Project Location..."
							onChangeInput={text => console.log(text)}
							selectText="Pick Location"
							styleMainWrapper={SignupStyles.MultiSelectMainWrapper}
							styleDropdownMenuSubsection={SignupStyles.MultiSelectDropdownMenu}
							displayKey="title"
							submitButtonText="Submit"
							underlineColorAndroid="transparent"
						/>
					</View>
					<TouchableOpacity onPress={this.CreateAccount} style={SignupStyles.ContinueTouchableOpacity}>
						<Text style={SignupStyles.SignupButton}>CONTINUE</Text>
					</TouchableOpacity>
					<View style={SignupStyles.SigninView}>
						<Text style={SignupStyles.SigninText}>Already have an account? Sign In</Text>
					</View>
				</ScrollView>
			</View>
		);
	}
}

export default SignupScreen;