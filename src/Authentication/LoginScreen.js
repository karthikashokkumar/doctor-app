import axios from "axios";
import React, { Component } from "react";
import RNRestart from 'react-native-restart';
import * as CONSTANT from '../Constants/Constant';
import AntIcon from "react-native-vector-icons/AntDesign";
import AsyncStorage from '@react-native-community/async-storage';
import { View, Text, TouchableOpacity, Image, Keyboard, TextInput, BackHandler, Modal, ActivityIndicator, StatusBar } from "react-native";
import LoginStyles from "../stylesheets/LoginStylesheet";

class LoginScreen extends Component {
	constructor(props) {
		super(props);
		this.state = this.getInitialState();
		this.handleSystemBackButton = this.handleSystemBackButton.bind(this);
	}

	getInitialState = () => ({
		username: "",
		password: "",
		isProgress: false,
		fetching_from_server: false
	});

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleSystemBackButton);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleSystemBackButton);
	}

	handleSystemBackButton() {
		this.props.navigation.goBack(null);
		return true;
	}

	login = () => {
		const { username, password } = this.state;
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (username == "") {
			this.setState({ email: "Please enter email address" });
		} else if (reg.test(username) === false) {
			this.setState({ email: "Invalid email address" });
			return false;
		} else if (password == "") {
			this.setState({ email: "Please enter password" });
		} else {
			this.setState({ fetching_from_server: true })
			axios
				.post(CONSTANT.BaseUrl + "user/do-login", { email: username, password: password })
				.then(async response => {
					if (response.status === 200) {
						console.log('Login Success');
						console.log('Response Data: ', JSON.stringify(response));
						const { type, profile } = response.data;
						const { id, profile_id } = profile.umeta;
						const { am_first_name, am_last_name, user_type, profile_img } = profile.pmeta;
						await AsyncStorage.setItem("full_name", am_first_name + " " + am_last_name);
						await AsyncStorage.setItem("user_type", user_type);
						await AsyncStorage.setItem("profile_img", profile_img);
						await AsyncStorage.setItem("profileType", type);
						await AsyncStorage.setItem("projectUid", JSON.stringify(id));
						await AsyncStorage.setItem("projectProfileId", JSON.stringify(profile_id));
						this.setState({ isProgress: false })
						console.log('App will get restarted');
						RNRestart.Restart();
						console.log('App restart check');
					} else {
						this.setState({ isProgress: false, fetching_from_server: false });
						alert("Please Check Your Email / Password or Check Network ");
					}
				})
				.catch(error => {
					console.error('Error Login: ', error);
				});
		}
		Keyboard.dismiss();
	};

	render() {
		return (
			this.state.isProgress ?
				<CustomProgressBar /> :
				<View style={{ flex: 1 }}>
					<StatusBar backgroundColor="#3d4461" barStyle="default" />
					<View style={LoginStyles.LoginHeaderView}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack(null)} style={LoginStyles.TouchableOpacity}>
							<AntIcon name="back" size={25} color={"#fff"} />
						</TouchableOpacity>
						<View style={LoginStyles.LoginHeaderParentView}>
							<View style={LoginStyles.LoginHeaderChildView} >
								<Text numberOfLines={1} style={LoginStyles.LoginHeaderText} >Sign In</Text>
							</View>
						</View>
					</View>
					<View style={LoginStyles.Container}>
						<Image style={LoginStyles.BannerLogo} source={require("../../Assets/Images/SplashImage.png")} />
						<View style={LoginStyles.LoginFormView}>
							<TextInput
								style={LoginStyles.TextInputStyle}
								underlineColorAndroid="transparent"
								name="username"
								placeholder={CONSTANT.LoginEmail}
								placeholderTextColor="#807f7f"
								onChangeText={username => this.setState({ username })}
							/>
							<View style={LoginStyles.BottomView} />
							<TextInput
								style={LoginStyles.TextInputStyle}
								underlineColorAndroid="transparent"
								editable={true}
								secureTextEntry={true}
								name="password"
								placeholder={CONSTANT.LoginPassword}
								placeholderTextColor="#807f7f"
								onChangeText={password => this.setState({ password })}
							/>
						</View>
						<TouchableOpacity
							onPress={this.login}
							style={LoginStyles.LoadMoreButton}>
							<Text style={LoginStyles.LoginButtonText}>SIGN IN</Text>
							{this.state.fetching_from_server == true ? (
								<ActivityIndicator color="white" style={{ marginLeft: 8 }} />
							) : null}
						</TouchableOpacity>
						<Text onPress={() => this.props.navigation.navigate("SignupScreen")} style={LoginStyles.SignupText}>Don't have an account? Sign Up</Text>
					</View>
				</View>
		);
	}
}

const CustomProgressBar = ({ visible }) => (
	<Modal onRequestClose={() => null} visible={visible}>
		<View style={LoginStyles.CustomProgressBarParentView}>
			<View style={LoginStyles.CustomProgressBarChildView}>
				<Text style={LoginStyles.CustomProgressBarText}>Loading...</Text>
				<ActivityIndicator size="large" color={CONSTANT.primaryColor} />
			</View>
		</View>
	</Modal>
);

export default LoginScreen;