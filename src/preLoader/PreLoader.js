import React, { Component } from "react";
import { View, StyleSheet, Image, AsyncStorage, ActivityIndicator, StatusBar } from "react-native";
class PreLoader extends Component {
	state = {
		storedValue: "",
		storedType: "",
		profileImg: "",
		type: "",
		showAlert: false
	};

	componentDidMount() {
		setTimeout(() => {
			this._checkUserLoginStatus();
		}, 1000);
	}

	_checkUserLoginStatus = async () => {
		try {
			const storedValue = await AsyncStorage.getItem("full_name");
			const storedType = await AsyncStorage.getItem("user_type");
			const profileImg = await AsyncStorage.getItem("profile_img");
			const type = await AsyncStorage.getItem("profileType");
			if (storedValue !== null) this.setState({ storedValue });
			if (storedType !== null) this.setState({ storedType });
			if (profileImg !== null) this.setState({ profileImg });
			if (type !== null) this.setState({ type });
			this.props.navigation.navigate("Home");
		} catch (error) {
			console.log(error);
			this.props.navigation.navigate("LoginScreen");
		}
	};

	render() {
		return (
			<View style={styles.container}>
				<StatusBar backgroundColor="#cc4641" barStyle="default" />
				<View style={styles.splashBackground}>
					<Image style={styles.splashImageStyle} source={require('../../Assets/Images/SplashImage.png')} />
					<ActivityIndicator style={styles.indicatorStyle} color="#fe736e" />
				</View>
			</View>
		);
	}
}

export default PreLoader;

const styles = StyleSheet.create({
	container: {
		flex: 1,
	}, splashBackground: {
		backgroundColor: '#e8f6ff',
		height: "100%",
	}, splashImageStyle: {
		justifyContent: 'center',
		alignSelf: 'center',
		top: '45%',
		width: 150,
		height: 100,
		resizeMode: "center"
	}, indicatorStyle: {
		top: '80%'
	}
});
