import React, { Component } from 'react';
import { View, Text, Image, } from 'react-native';
import AntIcon from 'react-native-vector-icons/AntDesign';
import { withNavigation } from 'react-navigation';
import ArticleCardStyles from '../stylesheets/ArticleCard';
class ArticlesCard extends Component {
	render() {
		return (
			<View style={ArticleCardStyles.container}>
				<View style={ArticleCardStyles.imageView}>
					<Image resizeMode={'cover'} style={{ height: 200 }} source={this.props.image} />
					<View style={{ padding: 15 }}>
						<Text style={{ color: '#55acee' }}>{this.props.category}</Text>
						<View style={ArticleCardStyles.parentView}>
							<Text numberOfLines={1} style={ArticleCardStyles.textView}>{this.props.title}</Text>
							<View style={ArticleCardStyles.childView}>
								<View
									style={ArticleCardStyles.iconView}>
									<AntIcon name="pushpino" color={'#3d4461'} size={12} />
									<Text style={ArticleCardStyles.iconText}>{this.props.date}</Text>
								</View>
								<View
									style={ArticleCardStyles.iconView}>
									<AntIcon name="hearto" color={'#3d4461'} size={12} />
									<Text style={ArticleCardStyles.iconText}>{this.props.likes}</Text>
								</View>
								<View
									style={ArticleCardStyles.iconView}>
									<AntIcon name="eyeo" color={'#3d4461'} size={12} />
									<Text style={ArticleCardStyles.iconText}>{this.props.views}</Text>
								</View>
								<View
									style={ArticleCardStyles.iconView}>
									<AntIcon name="sharealt" color={'#3d4461'} size={12} />
									<Text style={ArticleCardStyles.iconText}>Share</Text>
								</View>
							</View>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

export default withNavigation(ArticlesCard);
