import axios from "axios";
import React, { Component } from "react";
import * as Constants from '../config/Constants';
import { withNavigation } from 'react-navigation';
import CustomHeader from '../Header/CustomHeader';
import ImagePicker from 'react-native-image-crop-picker';
import AntIcon from "react-native-vector-icons/AntDesign";
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { View, StatusBar, ScrollView, Text, TouchableOpacity, TextInput, Image, FlatList, ActivityIndicator, Alert } from "react-native";
import ProfileStyles from "../stylesheets/ProfileStylesheet";
import { Picker } from "@react-native-picker/picker";

const Entities = require("html-entities").XmlEntities;
const entities = new Entities();

class PersonalDetail extends Component {

	constructor(props) {
		super(props);
		this.array = [];
		this.arrayExperience = [];
		this.arrayEducation = [];
		this.state = this.getInitialState();
	}

	getInitialState = () => ({
		base64_data: '',
		FirstName: '',
		LastName: '',
		DisplayName: '',
		BaseName: '',
		SubHeading: '',
		ShortDescription: '',
		Description: '',
		Address: '',
		Location: '',
		LocationList: [
			{
				"id": "1",
				"title": "Australia"
			},
			{
				"id": "2",
				"title": "Canada"
			},
			{
				"id": "3",
				"title": "England"
			},
			{
				"id": "4",
				"title": "India"
			},
			{
				"id": "5",
				"title": "Turkey"
			},
			{
				"id": "6",
				"title": "United Emirates"
			},
			{
				"id": "7",
				"title": "United Kingdom"
			},
			{
				"id": "8",
				"title": "United States"
			}
		],
		Latitude: '',
		Longitude: '',
		Registration: '',
		StratingPrice: '',
		Description: '',
		arrayHolder: [],
		itemHolder: [],
		items: [],
		ExperienceData: [],
		EducationData: [],
		AwardData: [],
		DownloadData: [],
		MembershipData: [],
		arrayHolder_Experience: [],
		arrayHolder_Education: [],
		AddExperienceForm: false,
		textInput_Holder: '',
		ViewExperienceForm: 'false',
		touchableOpacityHeight: 55,
		ExpCompanyName: '',
		ExpDescription: '',
		ExpEndDate: '',
		ExpStartingDate: '',
		ExpJobTitle: '',
		EduDescription: '',
		EduEndDate: '',
		EduInstituteName: '',
		EduInstituteTitle: '',
		EduStartingDate: '',
		AwardTitle: '',
		AwardYear: '',
		Memberdata: '',
		refreshing: true,
		awardrefresh: false,
		ExpRefresh: false,
		EduRefresh: false,
		DownloadRefresh: false,
		Memberfresh: false,
		isLoading: true,
		image: null,
		images: [],
		storedValue: "",
		storedType: "",
		profileImg: "",
		type: "",
		id: "",
		ProfileData: {},
		Mobile: ""
	});

	componentDidMount() {
		this.setState({ arrayHolder: [...this.array] })
		this.setState({ arrayHolder_Experience: [...this.arrayExperience] })
		this.getUser();
	}

	getUser = async () => {
		const storedValue = await AsyncStorage.getItem("full_name");
		const storedType = await AsyncStorage.getItem("user_type");
		const profileImg = await AsyncStorage.getItem("profile_img");
		const type = await AsyncStorage.getItem("profileType");
		const id = await AsyncStorage.getItem("projectUid");
		this.setState({ storedValue, storedType, profileImg, type, id });
		this.fetchProfileData();
	}

	fetchProfileData = async () => {
		const id = await AsyncStorage.getItem("projectUid");
		const response = await fetch(Constants.BaseUrl + "profile/setting?id=" + id);
		const json = await response.json();
		console.log("Profile Data", json);
		if (Array.isArray(json) && json[0] && json[0].type && json[0].type === 'error') {
			this.setState({ ProfileData: [] });
		} else {
			this.setState({ ProfileData: json });
			this.setState({ MembershipData: json.memberships });
			this.setState({ ExperienceData: json.am_experiences });
			this.setState({ EducationData: json.am_education });
			this.setState({ AwardData: json.am_award });
			this.setState({ DownloadData: json.am_downloads, isLoading: false });
			this.setState({ FirstName: json.am_first_name });
			this.setState({ LastName: json.am_last_name });
			this.setState({ Mobile: json.mobile });
			this.setState({ Location: json.location });
			this.setState({ DisplayName: json.display_name });
			this.setState({ BaseName: json.am_name_base });
			this.setState({ ShortDescription: json.am_short_description });
			this.setState({ SubHeading: json.am_sub_heading });
			this.setState({ Description: json.content });
			this.setState({ Address: json.address });
			this.setState({ Latitude: json.latitude });
			this.setState({ Longitude: json.longitude });
			this.setState({ Registration: json.reg_number });
			this.setState({ StratingPrice: json.am_starting_price });
			this.state.LocationList.map((e) => {
				if (e.title === json.location) 
				return this.setState({Location: e.id});
			});
		}
	};

	joinDataEducation = () => {
		this.arrayEducation.push({ titleExpe: "Add Education" });
		this.setState({ arrayHolder_Education: [...this.arrayEducation] });
	}

	joinDataExperience = () => {
		this.setState({ AddExperienceForm: true });
	}

	HandleEditForm = () => {
		this.setState({ ViewExperienceForm: "true" });
	}

	increaseHeight = () => {
		this.setState({ touchableOpacityHeight: 530 });
	}

	joinData = () => {
		this.array.push({ title: this.state.textInput_Holder });
		this.setState({ arrayHolder: [...this.array] });
	}

	FlatListItemSeparator = () => {
		return (<View style={ProfileStyles.FlatListItemSeperator} />);
	}

	removeItem = (key) => {
		let data = this.state.arrayHolder
		data = data.filter((item) => item.key !== key)
		this.setState({ data })
	}

	UpdateProfileData = async () => {
		const Uid = await AsyncStorage.getItem("projectUid");
		const role = await AsyncStorage.getItem("user_type");
		const { FirstName, LastName, Mobile, BaseName, SubHeading, Location, Address, Latitude, Longitude, ShortDescription, ExperienceData, EducationData, AwardData, Registration, DownloadData, MembershipData, StratingPrice } = this.state;
		if (this.state.FirstName === "") this.setState({ FirstName: this.state.ProfileData.am_first_name });
		if (this.state.LastName === "") this.setState({ FirstName: this.state.ProfileData.am_last_name });
		if (this.state.SubHeading === "") this.setState({ FirstName: this.state.ProfileData.am_sub_heading });

		if (role === 'regular') {
			axios.post(Constants.BaseUrl + "profile/store_profile_setting/regular", {
				user_id: Uid,
				first_name: FirstName,
				last_name: LastName,
				mobile: Mobile
			}).then((response) => {
				if (response.status === 200) {
					this.setState({ isUpdatingLoader: false });
					Alert.alert("Success", "Profile Updated Successfully");
				} else if (response.status === 203) {
					Alert.alert("Error", response.data.message);
				}
			}, (error) => {
				Alert.alert("Error", error.message)
				console.log(error);
			});
		}

		if (role === 'doctor') {
			axios.post(Constants.BaseUrl + "profile/store_profile_setting/doctor", {
				user_id: Uid,
				first_name: FirstName,
				last_name: LastName,
				mobile: Mobile,
				base_name: BaseName,
				subheading: SubHeading,
				starting_price: StratingPrice,
				address: Address
			}).then((response) => {
				if (response.status === 200) {
					this.setState({ isUpdatingLoader: false });
					Alert.alert("Success", "Profile Updated Successfully");
				} else if (response.status === 203) {
					Alert.alert("Error", response.data.message);
				}
			}, (error) => {
				Alert.alert("Error", error.message)
				console.log(error);
			});
		}

		if (role === 'hospital') {
			axios.post(Constants.BaseUrl + "profile/store_profile_setting/hospital", {
				user_id: Uid,
				first_name: FirstName,
				last_name: LastName,
				mobile: Mobile,
				location: Location,
				address: Address,
				longitude: Longitude,
				latitude: Latitude,
				registration_number: Registration,
				registration_document: "null"
			}).then((response) => {
				if (response.status === 200) {
					this.setState({ isUpdatingLoader: false });
					Alert.alert("Success", "Profile Updated Successfully");
				} else if (response.status === 203) {
					Alert.alert("Error", response.data.message);
				}
			}, (error) => {
				Alert.alert("Error", error.message)
				console.log(error);
			});
		}
	}

	pickMultiple() {
		DocumentPicker.pickMultiple({})
			.then(DownloadData => { this.setState({ image: null, images: DownloadData, DownloadRefresh: true, }); })
			.catch(e => alert(e));
	}

	fetchExperienceFormData = () => {
		if (this.state.ExpCompanyName == "" && this.state.ExpJobTitle == "" && this.state.ExpDescription == "") {
			Alert.alert('Please Enter The Data Properly');
		} else {
			this.state.ExperienceData.push({
				company_name: this.state.ExpCompanyName,
				start_date: this.state.ExpStartingDate,
				ending_date: this.state.ExpEndDate,
				job_title: this.state.ExpJobTitle,
				job_description: this.state.ExpDescription
			});
			this.setState({
				ExpRefresh: true,
			})
		}
	}

	fetchEducationFormData = () => {
		if (this.state.EduInstituteName == "" && this.state.EduInstituteTitle == "" && this.state.EduDescription == "") {
			Alert.alert('Please Enter The Data Properly');
		} else {
			this.state.EducationData.push({
				institute_name: this.state.EduInstituteName,
				start_date: this.state.EduStartingDate,
				ending_date: this.state.EduEndDate,
				degree_title: this.state.EduInstituteTitle,
				degree_description: this.state.EduDescription
			});
			this.setState({
				EduRefresh: true,
			})
		}
	}

	fetchAwardsData = () => {
		if (this.state.AwardTitle == "" && this.state.AwardYear) {
			Alert.alert('Please Enter The Data Properly');
		} else {
			this.state.AwardData.push({
				title: this.state.AwardTitle,
				year: this.state.AwardYear,
			});
			this.setState({
				awardrefresh: true,
			})
		}
	}

	fetchMembershipData = () => {
		if (this.state.Memberdata == "") {
			Alert.alert('Please Enter The Data Properly');
		} else {
			this.state.MembershipData.push({
				name: this.state.Memberdata,
			});
			this.setState({
				Memberfresh: true,
			})
		}
	}

	pickSingleProductBase64(cropit) {
		ImagePicker.openPicker({
			width: 300,
			height: 300,
			cropping: cropit,
			includeBase64: true,
			includeExif: true
		}).then(image => {
			this.setState({
				image: {
					uri: `data:${image.mime};base64,` + image.data,
					width: image.width,
					height: image.height
				},
				images: null
			});
			this.setState({
				base64_data: image.data
			})
		}).catch(e => console.log(e));
	}

	render() {
		console.log(this.state.Pro);
		const { isLoading } = this.state;
		const { storedType } = this.state;
		return (
			<View style={ProfileStyles.container}>
				<StatusBar backgroundColor='#3d4461' barStyle="default" />
				<CustomHeader headerText={'Profile Settings'} />
				{isLoading ? (
					<View style={ProfileStyles.ProfileLoadingView}>
						<ActivityIndicator
							size="small"
							color={Constants.primaryColor}
							style={ProfileStyles.ProfileLoadingActivityIndicator}
						/>
					</View>
				) : null}
				<ScrollView>
					<View style={ProfileStyles.ProfileScrollView}>
						<Text style={ProfileStyles.ProfileHeaderText}>Your Details</Text>
						{this.state.ProfileData && storedType == 'doctor' ?
							<TextInput
								defaultValue={`${entities.decode(this.state.ProfileData.am_name_base)}`}
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Select Gender"
								style={ProfileStyles.TextInputLayout}
								onChangeText={BaseName => this.setState({ BaseName })}>
							</TextInput>
							: storedType == 'doctor' ?
								<TextInput
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Select Gender"
									style={ProfileStyles.TextInputLayout}
								/>
								: null
						}
						{this.state.ProfileData && storedType == "doctor" ?
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Sub Heading"
								style={ProfileStyles.TextInputLayout}
								onChangeText={SubHeading => this.setState({ SubHeading })}>
								{`${entities.decode(this.state.ProfileData.am_sub_heading)}`}
							</TextInput>
							: storedType == "doctor" ?
								<TextInput
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Sub Heading"
									style={ProfileStyles.TextInputLayout} />
								: null
						}
						{this.state.ProfileData ?
							<TextInput
								defaultValue={`${entities.decode(this.state.ProfileData.am_first_name)}`}
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="First Name"
								style={ProfileStyles.TextInputLayout}
								onChangeText={FirstName => this.setState({ FirstName })}>
							</TextInput>
							:
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="First Name"
								style={ProfileStyles.TextInputLayout}>
							</TextInput>
						}
						{this.state.ProfileData ?
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Last Name"
								style={ProfileStyles.TextInputLayout}
								onChangeText={LastName => this.setState({ LastName })}>
								{`${entities.decode(this.state.ProfileData.am_last_name)}`}
							</TextInput>
							:
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Last Name"
								style={ProfileStyles.TextInputLayout}
							/>
						}
						{this.state.ProfileData ?
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Mobile No."
								style={ProfileStyles.TextInputLayout}
								onChangeText={Mobile => this.setState({ Mobile })}>
								{`${entities.decode(this.state.ProfileData.mobile)}`}
							</TextInput>
							:
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Mobile No."
								style={ProfileStyles.TextInputLayout}
							/>
						}
						{this.state.ProfileData && storedType == 'doctor' ?
							<TextInput
								multiline={true}
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Starting Price" style={ProfileStyles.TextInputLayoutContent}
								onChangeText={StratingPrice => this.setState({ StratingPrice })}>
								{`${entities.decode(this.state.ProfileData.am_starting_price)}`}
							</TextInput>
							: storedType == 'doctor' ?
								<TextInput
									multiline={true}
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Starting Price"
									style={ProfileStyles.TextInputLayoutContent}
								/>
								: null
						}
						{this.state.ProfileData && storedType == "doctor" ?
							<TextInput
								multiline={true}
								underlineColorAndroid="transparent"
								placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
								placeholder="Address"
								style={ProfileStyles.TextInputLayoutContent}
								onChangeText={Address => this.setState({ Address })}>
								{`${entities.decode(this.state.ProfileData.address)}`}
							</TextInput>
							: storedType == "doctor" ?
								<TextInput
									multiline={true}
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Address"
									style={ProfileStyles.DescriptionTextLayout}
								/>
								: null
						}
					</View>
					{
						storedType == "doctor" || storedType == "hospital" &&
						<View style={ProfileStyles.LocationView}>
							<Text style={ProfileStyles.LocationText}>Your Location</Text>
							{this.state.ProfileData ?
								<TouchableOpacity style={ProfileStyles.TextInput}>
									<Picker
										selectedValue={this.state.Location}
										onValueChange={value => this.setState({ Location: value })}>
										<Picker.Item label="Australia" value="1" color='' />
										<Picker.Item label="Canada" value="2" />
										<Picker.Item label="England" value="3" />
										<Picker.Item label="India" value="4" />
										<Picker.Item label="Turkey" value="5" />
										<Picker.Item label="UAE" value="6" />
										<Picker.Item label="UK" value="7" />
										<Picker.Item label="US" value="8" />
									</Picker>
								</TouchableOpacity>
								:
								<TouchableOpacity style={ProfileStyles.TextInput}>
									<Picker
										onValueChange={value => this.setState({ Location: value })}
										selectedValue={this.state.Location}>
										<Picker.Item label="Australia" value="1" />
										<Picker.Item label="Canada" value="2" />
										<Picker.Item label="England" value="3" />
										<Picker.Item label="India" value="4" />
										<Picker.Item label="Turkey" value="5" />
										<Picker.Item label="UAE" value="6" />
										<Picker.Item label="UK" value="7" />
										<Picker.Item label="US" value="8" />
									</Picker>
								</TouchableOpacity>
							}
							{this.state.ProfileData ?
								<TextInput
									multiline={true}
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Your Address"
									style={ProfileStyles.TextInputLayout}
									onChangeText={Address => this.setState({ Address })}>
									{`${entities.decode(this.state.ProfileData.address)}`}
								</TextInput>
								:
								<TextInput
									multiline={true}
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Your Address"
									style={ProfileStyles.TextInputLayout}
								/>}
							{this.state.ProfileData ?
								<TextInput
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Enter Longitude"
									style={ProfileStyles.TextInputLayout}
									onChangeText={Longitude => this.setState({ Longitude })}>
									{`${entities.decode(this.state.ProfileData.longitude)}`}
								</TextInput>
								:
								<TextInput
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Enter Longitude" style={ProfileStyles.TextInputLayout}
								/>}
							{this.state.ProfileData ?
								<TextInput
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Enter Latitude"
									style={ProfileStyles.TextInputLayout}
									onChangeText={Latitude => this.setState({ Latitude })}>
									{`${entities.decode(this.state.ProfileData.latitude)}`}
								</TextInput>
								:
								<TextInput
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Enter Latitude"
									style={ProfileStyles.TextInputLayout}
								/>}
						</View>
					}
					<View style={ProfileStyles.UploadParentView}>
						<Text style={ProfileStyles.UploadText}>Profile Photo</Text>
						<View res style={ProfileStyles.UploadChildView}>
							<TouchableOpacity onPress={() => this.pickSingleProductBase64(false)} style={ProfileStyles.UploadTouchableOpacity}>
								<View style={ProfileStyles.AddFileView}>
									<AntIcon onPress={this.joinData}
										name="plus"
										color={"#767676"}
										size={27}
									/>
									<Text style={ProfileStyles.AddFileText}>Add Profile Photo</Text>
								</View>
							</TouchableOpacity>
							{this.state.ProfileData ?
								<Image
									style={ProfileStyles.File}
									source={{ uri: `${this.state.ProfileData.profile_image_url}` }}
								/>
								:
								<Image
									style={ProfileStyles.File}
									source={{ uri: `${this.state.fetchImages >= 1 ? this.state.fetchImages : Constants.DefaultProfilePicture} ` }}
								/>
							}
						</View>
					</View>
					{storedType == 'doctor' &&
						<View style={ProfileStyles.MembershipParentView}>
							<Text style={ProfileStyles.MembershipText}>Membership</Text>
							<View style={ProfileStyles.MembershipChildView}>
								<TextInput
									onChangeText={Memberdata => this.setState({ textInput_Holder: Memberdata })}
									underlineColorAndroid="transparent"
									placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
									placeholder="Enter Value Here"
									style={ProfileStyles.MembershipTextInput}
								/>
								<View style={ProfileStyles.MembershipIconView}>
									<AntIcon
										onPress={() => this.fetchMembershipData()}
										name="plus"
										color={"#fff"}
										size={20}
										style={ProfileStyles.MembershipTop}
									/>
								</View>
							</View>
							{this.state.MembershipData ?
								<FlatList
									style={{ paddingLeft: 5 }}
									data={this.state.MembershipData}
									extraData={this.state.Memberfresh}
									renderItem={({ item }) =>
										<View style={ProfileStyles.MembershipDataView}>
											<Text style={ProfileStyles.MembershipDataText}>{item.title}</Text>
										</View>
									}
								/>
								:
								null
							}
						</View>
					}
					{storedType == "doctor" &&
						<View>
							<View style={ProfileStyles.ParentView}>
								<View style={ProfileStyles.ChildView}>
									<Text style={ProfileStyles.Text}>Experience</Text>
									<TouchableOpacity
										onPress={() => this.joinDataExperience()}
										style={ProfileStyles.ExperienceTouchableOpacity}>
									</TouchableOpacity>
								</View>
								<View style={ProfileStyles.TextInputParentView}>
									<View style={ProfileStyles.TextInputChildView}>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="Company Name"
											onChangeText={ExpCompanyName => this.setState({ ExpCompanyName })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="Starting Date"
											onChangeText={ExpStartingDate => this.setState({ ExpStartingDate })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="End Date"
											onChangeText={ExpEndDate => this.setState({ ExpEndDate })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											onChangeText={data => this.setState({ textInput_Holder: data })}
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											underlineColorAndroid="transparent"
											placeholder="Job Title"
											onChangeText={ExpJobTitle => this.setState({ ExpJobTitle })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="Description"
											onChangeText={ExpDescription => this.setState({ ExpDescription })}
											style={ProfileStyles.TextInput}
										/>
										<TouchableOpacity
											onPress={this.fetchExperienceFormData}
											style={ProfileStyles.buttonHover}>
											<Text style={ProfileStyles.AddNowButton}>Add Now</Text>
										</TouchableOpacity>
									</View>
									{this.state.ExperienceData ?
										<FlatList
											style={{ paddingLeft: 5 }}
											data={this.state.ExperienceData}
											extraData={this.state.ExpRefresh}
											renderItem={({ item }) =>
												<Collapse >
													<CollapseHeader style={ProfileStyles.CollapseHeader}>
														<View style={ProfileStyles.CollapseHeaderView}>
															<Text style={ProfileStyles.CollapseHeaderText}>{item.company_name}</Text>
															<View style={ProfileStyles.EditIconView}>
																<AntIcon onPress={() => this.HandleEditForm()} name="edit" color={"#fff"} size={20} style={{ top: 15 }} />
															</View>
															<View style={ProfileStyles.DeleteIconView}>
																<AntIcon name="delete" color={"#fff"} size={20} style={{ top: 15 }} />
															</View>
														</View>
													</CollapseHeader>
													<CollapseBody >
														<View style={{ marginTop: 10 }}>
															<TextInput
																underlineColorAndroid="transparent"
																placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																placeholder="Company Name"
																style={ProfileStyles.TextInput}>
																{item.company_name}
															</TextInput>
															<TextInput
																underlineColorAndroid="transparent"
																placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																placeholder="Starting Date"
																style={ProfileStyles.TextInput}
															>{item.start}</TextInput>
															<TextInput
																underlineColorAndroid="transparent"
																placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																placeholder="End Date"
																style={ProfileStyles.TextInput}
															>{item.ending}</TextInput>
															<TextInput
																onChangeText={data => this.setState({ textInput_Holder: data })}
																placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																underlineColorAndroid="transparent"
																placeholder="Job Title"
																style={ProfileStyles.TextInput}
															>{item.job_title}</TextInput>
															<TextInput
																multiline={true}
																underlineColorAndroid="transparent"
																placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																placeholder="Description"
																style={ProfileStyles.TextInput}
															>{item.description}</TextInput>
															<TouchableOpacity
																onPress={this.joinData}
																style={ProfileStyles.buttonHover}>
																<Text style={ProfileStyles.AddNowButton}>Add Now</Text>
															</TouchableOpacity>
														</View>
													</CollapseBody>
												</Collapse>
											}
										/>
										: null
									}
								</View>
							</View>
							<View style={ProfileStyles.ParentView}>
								<View style={ProfileStyles.ChildView}>
									<Text style={ProfileStyles.Text}>Education</Text>
									<TouchableOpacity
										onPress={() => this.joinDataEducation()}
										style={ProfileStyles.TouchableOpacity}>
									</TouchableOpacity>
								</View>
								<View style={ProfileStyles.TextInputParentView}>
									<View style={ProfileStyles.TextInputChildView}>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="Institute Name"
											onChangeText={EduInstituteName => this.setState({ EduInstituteName })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="Starting Date"
											onChangeText={EduStartingDate => this.setState({ EduStartingDate })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="End Date"
											onChangeText={EduEndDate => this.setState({ EduEndDate })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											onChangeText={data => this.setState({ textInput_Holder: data })}
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											underlineColorAndroid="transparent"
											placeholder="Institute Title"
											onChangeText={EduInstituteTitle => this.setState({ EduInstituteTitle })}
											style={ProfileStyles.TextInput}
										/>
										<TextInput
											underlineColorAndroid="transparent"
											placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
											placeholder="Description"
											onChangeText={EduDescription => this.setState({ EduDescription })}
											style={ProfileStyles.TextInput}
										/>
										<TouchableOpacity onPress={this.fetchEducationFormData} style={ProfileStyles.buttonHover}>
											<Text style={ProfileStyles.AddNowButton}>Add Now</Text>
										</TouchableOpacity>
									</View>
									{
										this.state.EducationData ?
											<FlatList
												style={{ paddingLeft: 5 }}
												data={this.state.EducationData}
												extraData={this.state.EduRefresh}
												renderItem={({ item }) =>
													<Collapse >
														<CollapseHeader style={ProfileStyles.CollapseHeader}>
															<View style={ProfileStyles.CollapseHeaderView}>
																<Text style={ProfileStyles.CollapseHeaderText}>{item.institute_name}</Text>
																<View style={ProfileStyles.EditIconView}>
																	<AntIcon onPress={() => this.HandleEditForm()} name="edit" color={"#fff"} size={20} style={{ top: 15 }} />
																</View>
																<View style={ProfileStyles.DeleteIconView}>
																	<AntIcon name="delete" color={"#fff"} size={20} style={{ top: 15 }} />
																</View>
															</View>
														</CollapseHeader>
														<CollapseBody >
															<View style={{ marginTop: 10 }}>
																<TextInput
																	underlineColorAndroid="transparent"
																	placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																	placeholder="Institute Name"
																	style={ProfileStyles.TextInput}
																>{item.institute_name}</TextInput>
																<TextInput
																	underlineColorAndroid="transparent"
																	placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																	placeholder="Starting Date"
																	style={ProfileStyles.TextInput}
																>{item.start}</TextInput>
																<TextInput
																	underlineColorAndroid="transparent"
																	placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																	placeholder="End Date"
																	style={ProfileStyles.TextInput}
																>{item.ending}</TextInput>
																<TextInput
																	onChangeText={data => this.setState({ textInput_Holder: data })}
																	placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																	underlineColorAndroid="transparent"
																	placeholder="Degree Title"
																	style={ProfileStyles.TextInput}
																>{item.degree_title}</TextInput>
																<TextInput
																	multiline={true}
																	underlineColorAndroid="transparent"
																	placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
																	placeholder="Description"
																	style={ProfileStyles.TextInput}
																>{item.edu_desc}</TextInput>
																<TouchableOpacity
																	onPress={this.joinData}
																	style={ProfileStyles.buttonHover}>
																	<Text style={ProfileStyles.AddNowButton}>Add Now</Text>
																</TouchableOpacity>
															</View>
														</CollapseBody>
													</Collapse>
												}
											/>
											: null
									}
								</View>
							</View>
						</View>
					}
					{storedType == 'doctor' &&
						<View style={ProfileStyles.ParentView}>
							<View style={ProfileStyles.ChildView}>
								<Text style={ProfileStyles.Text}>Add Your Awards</Text>
								<TouchableOpacity
									onPress={() => this.joinDataEducation()}
									style={ProfileStyles.TouchableOpacity}>
								</TouchableOpacity>
							</View>
							<View style={ProfileStyles.TextInputParentView}>
								<View style={ProfileStyles.TextInputChildView} >
									<TextInput
										underlineColorAndroid="transparent"
										placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
										placeholder="Award Title"
										onChangeText={AwardTitle => this.setState({ AwardTitle })}
										style={ProfileStyles.TextInput}
									/>
									<TextInput
										underlineColorAndroid="transparent"
										placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
										placeholder="Year"
										onChangeText={AwardYear => this.setState({ AwardYear })}
										style={ProfileStyles.TextInput}
									/>
									<TouchableOpacity
										onPress={this.fetchAwardsData}
										style={ProfileStyles.buttonHover}
									>
										<Text style={ProfileStyles.AddNowButton}>Add Now</Text>
									</TouchableOpacity>
								</View>
								{this.state.AwardData ?
									<FlatList
										style={{ paddingLeft: 5 }}
										data={this.state.AwardData}
										extraData={this.state.awardrefresh}
										renderItem={({ item }) =>
											<Collapse >
												<CollapseHeader style={ProfileStyles.CollapseHeader}>
													<View style={ProfileStyles.CollapseHeaderView}>
														<Text style={ProfileStyles.CollapseHeaderText}>{item.title}</Text>
														<View style={ProfileStyles.EditIconView}>
															<AntIcon onPress={() => this.HandleEditForm()} name="edit" color={"#fff"} size={20} style={{ top: 15 }} />
														</View>
														<View style={ProfileStyles.DeleteIconView}>
															<AntIcon name="delete" color={"#fff"} size={20} style={{ top: 15 }} />
														</View>
													</View>
												</CollapseHeader>
												<CollapseBody >
													<View style={{ marginTop: 10 }}>
														<TextInput
															underlineColorAndroid="transparent"
															placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
															placeholder="Award Title"
															onChangeText={AwardTitle => this.setState({ AwardTitle })}
															style={ProfileStyles.TextInput}
														>{item.title}</TextInput>
														<TextInput
															underlineColorAndroid="transparent"
															placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
															placeholder="Award Year"
															onChangeText={AwardYear => this.setState({ AwardYear })}
															style={ProfileStyles.TextInput}
														>{item.year}</TextInput>

														<TouchableOpacity
															onPress={this.fetchAwardsData}
															style={ProfileStyles.buttonHover}>
															<Text style={ProfileStyles.AddNowButton}>Add Now</Text>
														</TouchableOpacity>
													</View>
												</CollapseBody>
											</Collapse>
										}
									/>
									: null
								}
							</View>
						</View>
					}
					{storedType == "doctor" &&
						<View style={ProfileStyles.UploadParentView}>
							<View style={ProfileStyles.ChildView}>
								<Text style={ProfileStyles.Text}>Downloads</Text>
								<TouchableOpacity
									onPress={() => this.joinDataEducation()}
									style={ProfileStyles.TouchableOpacity}>
								</TouchableOpacity>
							</View>
							<View style={ProfileStyles.AddFileView}>
								<TouchableOpacity onPress={() => this.pickMultiple()} style={ProfileStyles.UploadTouchableOpacity}>
									<View style={ProfileStyles.AddFileView}>
										<AntIcon name="plus" color={"#767676"} size={27} />
										<Text style={ProfileStyles.AddFileText}>Add Files for Download</Text>
									</View>
								</TouchableOpacity>
								{this.state.DownloadData ?
									<FlatList
										style={{ paddingLeft: 5 }}
										data={this.state.DownloadData}
										extraData={this.state.DownloadRefresh}
										renderItem={({ item }) =>
											<View style={ProfileStyles.CollapseHeader}>
												<View style={ProfileStyles.DocumentView}>
													<Image resizeMode={"cover"} style={ProfileStyles.DocumentIcon} source={require('../../Assets/Images/Download.png')} />
												</View>
												<View style={ProfileStyles.DocumentTextView}>
													<Text style={ProfileStyles.DocumentText}>{item.name} {"\n"}{item.size}</Text>
												</View>
												<View style={ProfileStyles.DeleteIconView}>
													<AntIcon name="delete" color={"#fff"} size={20} style={{ top: 15 }} />
												</View>
											</View>
										}
									/>
									: null
								}
							</View>
						</View>
					}
					{storedType == 'doctor' || storedType == 'hospital' &&
						<View style={ProfileStyles.ParentView}>
							<View style={ProfileStyles.ChildView}>
								<Text style={ProfileStyles.Text}>Add Your Registration No.</Text>
								<TouchableOpacity
									onPress={() => this.joinDataEducation()}
									style={ProfileStyles.TouchableOpacity}>
								</TouchableOpacity>
							</View>
							<View style={ProfileStyles.TextInputParentView}>
								{this.state.ProfileData ?
									<TextInput
										underlineColorAndroid="transparent"
										placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
										placeholder="Enter Registration Number"
										style={ProfileStyles.TextInputLayout}
										onChangeText={Registration => this.setState({ Registration })}>
										{`${entities.decode(this.state.ProfileData.reg_number)}`}
									</TextInput>
									:
									<TextInput
										underlineColorAndroid="transparent"
										placeholderTextColor={ProfileStyles.PlaceholderTextColor.color}
										placeholder="Enter Registration Number"
										style={ProfileStyles.TextInputLayout}
									/>
								}
								<View style={ProfileStyles.AddDocumentView}>
									<View style={ProfileStyles.AddFileView}>
										<AntIcon onPress={this.joinData} name="plus" color={"#767676"} size={27} />
										<Text style={{ color: '#767676', fontSize: 17 }}>Add Document</Text>
									</View>
								</View>
								{this.state.ProfileData ?
									<View style={ProfileStyles.CollapseHeaderView}>
										<View style={ProfileStyles.DocumentView}>
											<Image resizeMode={"cover"} style={ProfileStyles.DocumentIcon} source={require('../../Assets/Images/Download.png')} />
										</View>
										<View style={ProfileStyles.DocumentTextView}>
											<Text style={ProfileStyles.DocumentText}>{this.state.ProfileData.document_name} {"\n"}File Size: {this.state.ProfileData.document_size}</Text>
										</View>
										<View style={ProfileStyles.DeleteIconView}>
											<AntIcon name="delete" color={"#fff"} size={20} style={{ top: 15 }} />
										</View>
									</View>
									: null
								}
							</View>
						</View>
					}

					<TouchableOpacity onPress={this.UpdateProfileData} style={ProfileStyles.UpdateButtonTouchableOpacity}>
						<Text style={ProfileStyles.UpdateButtonText}>Update Profile</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

export default withNavigation(PersonalDetail);