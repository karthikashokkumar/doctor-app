import React, { Component } from "react";
import { withNavigation } from "react-navigation";
import ListStyles from '../stylesheets/AddListCard';
import AntIcon from "react-native-vector-icons/AntDesign";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";

class AddListCard extends Component {
	constructor(props) {
		super(props);
		(this.array = []),
			(this.state = {
				arrayHolder: [],
				textInput_Holder: ""
			});
	}

	componentWIllMount() {
		this.setState({ arrayHolder: [...this.array] });
	}

	joinData = () => {
		this.array.push({ title: this.state.textInput_Holder });
		this.setState({ arrayHolder: [...this.array] });
	};

	FlatListItemSeparator = () => {
		return (
			<View
				style={{
					height: 1,
					width: "100%",
					backgroundColor: "#607D8B",
				}}
			/>
		);
	};

	GetItem(item) {
		Alert.alert(item.title);
	}

	removeItem = key => {
		let data = this.state.arrayHolder;
		data = data.filter(item => item.key !== key);
		this.setState({ data });
	};

	render() {
		return (
			<View style={ListStyles.container}>
				<Collapse >
					<CollapseHeader style={ListStyles.collapseHeader}>
						<View style={ListStyles.textView}>
							<Text style={textExperience}>Add Experience</Text>
							<View style={ListStyles.editIconView}>
								<AntIcon on onPress={() => this.HandleEditForm()} name="edit" color={"#fff"} size={20} style={{ top: 15 }} />
							</View>
							<View style={ListStyles.deleteIconView}>
								<AntIcon name="delete" color={"#fff"} size={20} style={{ top: 15 }} />
							</View>
						</View>
					</CollapseHeader>
					<CollapseBody>
						<View style={{ height: 500 }}>
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor="#7F7F7F"
								placeholder="Company Name"
								style={ListStyles.textInput}
							/>
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor="#7F7F7F"
								placeholder="Starting Date"
								style={ListStyles.textInput}
							/>
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor="#7F7F7F"
								placeholder="End Date"
								style={ListStyles.textInput}
							/>
							<TextInput
								onChangeText={data => this.setState({ textInput_Holder: data })}
								placeholderTextColor="#7F7F7F"
								underlineColorAndroid="transparent"
								placeholder="Job Title"
								style={ListStyles.textInput}
							/>
							<TextInput
								underlineColorAndroid="transparent"
								placeholderTextColor="#7F7F7F"
								placeholder="Description"
								style={ListStyles.descriptionTextInput}
							/>
							<TouchableOpacity onPress={this.joinData} style={ListStyles.buttonHover}>
								<Text style={ListStyles.touchableOpacity}>Add Now</Text>
							</TouchableOpacity>
						</View>
					</CollapseBody>
				</Collapse>
			</View>
		);
	}
}

export default withNavigation(AddListCard);